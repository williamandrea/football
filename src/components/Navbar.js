import React, {useState, useEffect} from 'react'
import M from 'materialize-css/dist/js/materialize'

const li = ['League Standings', 'My Teams']

const Navbar = () => {
  const pathName = window.location.pathname.substr(1)

  useEffect(() => {
    M.AutoInit()

  }, [])

  return (
    <nav className = 'black' role = 'navigation'>
      <div className = 'nav-wrapper container'>
        <a href = "/leaguestandings" className = 'brand-logo bold center flow-text hide-on-small-only' id = 'logo-container'>
          Soccer <span className = 'orange black-text logo-card flow-text'>hub</span>
        </a>
        <a href = "#" className = 'sidenav-trigger orange-text' data-target = 'nav-mobile'>☰</a>      
        <ul className = 'right hide-on-med-and-down'>
          {
            li.map((item, key) => {
              return (
                <li className = {pathName === item.replace(/\s/g, '').toLowerCase()? 'orange': null} key = {key}>
                  <a href = {`/${item.replace(/\s/g, '').toLowerCase()}`} className = 'waves-effect white-text'>{item}</a>
                </li>
              )
            })
          }
        </ul>
        <ul className = 'sidenav black' id = 'nav-mobile'>
          {
            li.map((item, key) => (
              <li className = {pathName === item.replace(/\s/g, '').toLowerCase()? 'orange': null} key = {key}>
                <a href = {`/${item.replace(/\s/g, '').toLowerCase()}`} className = 'waves-effect white-text'>{item}</a>
              </li>
            ))
          }
        </ul>
      </div>
    </nav>
  )
}

export default Navbar