import React from 'react'
import ErrorPic from '../img/error.png'

const Row = (props) => (
  <tr className = 'tr-standings grey-text text-darken-2' onClick = {() => props.handleRowClick(props.id)}>
    <td className = 'team-position bold'>{props.position}</td>
    <td className = 'team-name'>
      <div className = 'crest hide-on-med-and-down'>
        <img className = 'responsive-img' src = {props.crestUrl.replace(/^http:\/\//i, 'https://')} />
      </div>
      <span className = 'bold'>{props.name}</span>
    </td>
    <td className = ''>{props.playedGames}</td>
    <td className = ''>{props.won}</td>
    <td className = ''>{props.draw}</td>
    <td className = ''>{props.lost}</td>
    <td className = ''>{props.goalsFor}</td>
    <td className = ''>{props.goalsAgainst}</td>
    <td className = {props.goalDifference < 0 ? 'red-text darken-4': 'green-text'}>{props.goalDifference}</td>
    <td className = 'team-points bold'>{props.points}</td>
  </tr>
)

export default Row