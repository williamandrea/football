import React from 'react'

const Table = (props) => (
  <>
    <h5>{props.title}</h5>
    <table className = {props.responsive? 'responsive-table centered highlight': 'centered highlight'}>
      <thead className = 'black white-text'>
        <tr>
          {
            props.th? (
              props.th.map((item, key) => (
                <th key = {key} className = 'bold'>{item}</th>
              ))
            ): null
          }
        </tr>
      </thead>
      <tbody>
        {props.children}
      </tbody>
    </table>
  </>
)

export default Table