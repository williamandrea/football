import React from 'react'
import Bundesliga from '../img/Bundesliga.png'
import Eredivisie from '../img/Eredivisie.png';
import LaLiga from '../img/LaLiga.png';
import Ligue1 from '../img/Ligue1.png';
import PremierLeague from '../img/PremierLeague.png';
import SerieA from '../img/SerieA.png';
import ErrorPic from '../img/error.png'

const LeagueInfo = (props) => {
  let src = ''
  switch (props.caption) {
    case 'Bundesliga':
      src = Bundesliga
      break
    case 'Eredivisie':
      src =  Eredivisie
      break
    case "Primera Division":
      src = LaLiga
      break
    case "Ligue 1":
      src = Ligue1
      break
    case "Premier League":
      src = PremierLeague;
      break
    case "Serie A":
      src = SerieA
      break
    default:
      src = LaLiga
      break
  }
  return (
    <div className = 'flex-row bottom-border'>
      <img src = {src} className = 'responsive-img' />
    </div>
  )
}

export default LeagueInfo