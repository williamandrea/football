import React from 'react'

const SquadRow = (props) => (
  <>
    {
      props.squad.map((item, key) => (
        <tr className = 'grey-text text-darken-2' key = {key}>
          <td>{key + 1}</td>
          <td>{item.name}</td>
          <td>{item.role}</td>
          <td>{item.position || '-'}</td>
          <td>{item.nationality}</td>
        </tr>
      ))
    }
  </>
)

export default SquadRow