import React from 'react'
import ErrorPic from '../img/error.png'

const MyTeamsRow = (props) => (
  props.data.length > 0? (
    props.data.map((item, key) => (
      <tr className = 'bold grey-text text-darken-2' key = {key} onClick = {() => props.handleRowClick(item.id)}>
        <td>{key + 1}</td>
        <td className = 'team-name'>
          <div className = 'crest'>
            <img className = 'responsive-img' src = {item.crestUrl.replace(/^http:\/\//i, 'https://')} />
          </div>
          <p className = 'left-align'>
            {item.name}
          </p>
        </td>
      </tr>
    ))
  ): (
    <tr className = 'bold grey-text text-darken-2'>
      <td> </td>
      <th>Empty</th>
    </tr>
  )
) 


export default MyTeamsRow