import React from 'react'
import {ligaList} from '../data/const'

const Select = (props) => (
  <div className = 'input-field col'>
    <select defaultValue = {props.leagueID} onChange = {props.handleSelect}>
      {
        ligaList.map((item, key) => (
          <option key = {key} value = {item.id}>{item.name}</option>
        ))
      }
    </select>
  </div>
)

export default Select