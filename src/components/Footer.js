import React from 'react'

const Footer = () => (
  <footer className = 'footer-copyright black'>
    <div className = 'container center white-text'>
      © 2020 Copyright Soccerhub
    </div>
  </footer>
)

export default Footer