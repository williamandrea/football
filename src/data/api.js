import {apiURL} from './const'

const status = (response) => {
  if (response.status !== 200) {
    console.log('Status error', response.status)
    return Promise.reject(new Error(response.statusText))
  }else {
    return Promise.resolve(response)
  }
}

const json = (response) => {
  return reponse.json
}

const error = (error) => {
  console.log('error', error)
}

const getTeams = () => {
  if ('cache' in window) {
    caches.match(apiURL + 'teams')
      .then((response) => {
        if (response) {
          response.json.then((data) => {
            return data
          })
        }
      })
  }
  fetch(apiURL + 'teams')
    .then(status)
    .then(json)
    .then((data) => {
      return data
    })
    .catch(error)
}

const getTeamById = () => {
  return new Promise((resolve, reject) => {
    const urlParams = new URLSearchParams(window.location.search)
    const idParam = urlParam.get('id')
    if ('caches' in window) {
      catches.match(url + 'teams/' + idParam)
        .then((response) => {
          response.json().then((data) => {
            resolve(data)
          })
        })
    }
  })
  fetch(apiURL + 'teams/' + idParam)
    .then(status)
    .then(json)
    .then((data) => {
      resolve(data)
    })
}

const getSavedTeams = () => {
  getAll().then((teams) => {
    return teams
  })
}