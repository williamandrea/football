import idb from 'idb'

export const dbPromised = idb.open('myteams', 2, (upgradeDb) => {
  const objectStore = upgradeDb.createObjectStore('team', {
    keyPath: 'id'
  })
  objectStore.createIndex('teamName', 'teamName', {
    unique: true
  })
})

export const saveTeams = (teams) => {
  dbPromised.then((db) => {
    const tx = db.transaction('team', 'readwrite')
    const store = tx.objectStore('team')
    store.add(teams)
    return tx.complete
  }).then(() => {
    console.log('Tim berhasil disimpan')
  })
}

export const getAll = () => {
  return new Promise((resolve, reject) => {
    dbPromised.then((db) => {
      const tx = db.transaction('team', 'readonly')
      const store = tx.objectStore('team')
      return store.getAll()
    }).then((teams) => {
      resolve(teams)
    })
  })
}

export const checkExistTeam = (key) => {
  return new Promise((resolve, reject) => {
    dbPromised.then((db) => {
      const req = db.transaction('team').objectStore('team').count(key)
      resolve(req)
    })
  })
}

