export const apiKey = '6cfd487d7bf34c57a9547a3dcc0c51da'
export const apiURL = 'https://api.football-data.org/v2'

export const ligaList = [
  {
    id: 'PD',
    name: 'La Liga',
  },
  {
    id: 'PL',
    name: 'Premier League',
  },
  {
    id: 'DED',
    name: 'Eredivisie',
  },
  {
    id: 'FL1',
    name: 'Ligue 1',
  },
  {
    id: 'BL1',
    name: 'Bundesliga',
  },
  {
    id: 'SA',
    name: 'Serie A',
  },
]