import React, {useState, useEffect} from 'react'
import axios from 'axios'
import {apiURL, apiKey} from '../data/const'
import {Select, LeagueInfo, StandingRow, Table} from '../components'
import M from 'materialize-css/dist/js/materialize.min'
import ReactLoading from 'react-loading'
import {useHistory} from 'react-router-dom'

const th = ['#', 'Team', 'MP', 'W', 'D', 'L', 'GF', 'GA', 'GD', 'Pts']

const Standings = () => {
  const history = useHistory()
  const [leagueID, setLeagueID] = useState('PD')
  const [leagueName, setLeagueName] = useState('La Liga')
  const [rows, setRows] = useState([])
  const [isLoading, setLoading] = useState(false)

  useEffect(() => {
    window.scrollTo(0, 0)
    M.AutoInit()
  }, [])

  useEffect(() => {
    getStandings()
  }, [leagueID])

  const getStandings = () => {
    setLoading(true)
    axios.get(`${apiURL}/competitions/${leagueID}/standings`, {
      headers: {'X-Auth-Token': apiKey}
    }).then((result) => {
      let filteredRows = []
      result.data.standings[0].table.map((item, key) => {
        const {position, playedGames, won, draw, lost, goalsFor, goalsAgainst, goalDifference, points} = item
        let {id, crestUrl, name} = item.team
        crestUrl = crestUrl.replace(/^http:\/\//i, 'https://');
        return (
          filteredRows.push(
            <StandingRow 
              key = {key} id = {id} position = {position} 
              crestUrl = {crestUrl} name = {name} 
              playedGames = {playedGames} won = {won} 
              draw = {draw} lost = {lost} goalsFor = {goalsFor} 
              goalsAgainst = {goalsAgainst} goalDifference = {goalDifference} 
              points = {points} handleRowClick = {handleRowClick} />
          )
        )
      })
      setLeagueName(result.data.competition.name)
      setRows(filteredRows)
    }).catch((error) => {
      console.log(error)
    }).finally(() => {
      setLoading(false)
    })
  }

  const handleSelect = (event) => {
    setLeagueID(event.target.value)
    document.querySelector('table').focus()
  }

  const handleRowClick = (id) => {
    history.push(`/teams/${id}`)
  }

  return (
    <>
      <h2 id = 'page-title' className = 'header black-text'>2020 Featured Soccer Standings</h2>
      <div className = 'divider' />

      <Select leagueID = {leagueID} handleSelect = {handleSelect} />
      {
        isLoading? 
        <ReactLoading className = 'center-align' type = 'bubbles' color = 'black' />:
        <div className = 'section'>
          <LeagueInfo caption = {leagueName} />
          <Table th = {th} responsive = {true}>
            {rows}
          </Table>
        </div>
      }
    </>
  )
}

export default Standings