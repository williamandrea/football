import React, {useState, useEffect} from 'react'
import {apiURL, apiKey} from '../data/const'
import {Table, SquadRow} from '../components'
import axios from 'axios'
import ReactLoading from 'react-loading'
import {saveTeams, checkExistTeam} from '../data/db'

const th = ['#', 'Name', 'Role', 'Position', 'Nationality']

const Teams = (id) => {
  const [isLoading, setLoading] = useState(false)
  const [data, setData] = useState({})
  const [squad, setSquad] = useState([])
  const [isExistTeam, setExistTeam] = useState(false)

  useEffect(() => {
    window.scrollTo(0, 0)
    getTeams()
    checkExistTeam(id.match.params.id).then((res) => {
      setExistTeam(res)
    })
  }, [])

  const getTeams = () => {
    setLoading(true)
    axios.get(`${apiURL}/teams/${id.match.params.id}`, {
      headers: {'X-Auth-Token': apiKey}
    }).then((result) => {
      let {name, address, phone, website, email, founded, crestUrl, squad} = result.data
      crestUrl =  crestUrl.replace(/^http:\/\//i, 'https://');
      setSquad(squad)
      const temp = {id: id.match.params.id, name, address, phone, website, email, founded, crestUrl, squad}
      setData(temp)
    }).catch((error) => {
      console.log(error)
    }).finally(() => {
      setLoading(false)
    })
  }

  const showNotification = () => {
    const title = 'Soccer Hub'
    const options = {
      'body': `${data.name} successfully added to My Teams..`,
      'icon': 'icon.png',
      'badge': 'icon.png',
    }

    if (Notification.permission === 'granted') {
      navigator.serviceWorker.ready.then((registration) => {
        registration.showNotification(title, options)
      })
    }else {
      console.log('Notifikasi tidak diizinkan..')
    }
  }

  const addToMyTeams = () => {
    saveTeams(data)
    setExistTeam(true)
    showNotification()
  }

  return (
    <>
      <h2 className = 'black-text'>Teams</h2>
      <div className = 'divider' />
      {
        isLoading?
        <ReactLoading className = 'center-align' type = 'bubbles' color = 'black' />:
        <div className = 'section'>
          {
            !isExistTeam ? 
            <button onClick = {addToMyTeams} className = 'waves-effect waves-light btn green accent-4'>Add to My Teams</button> :
            <button className = 'waves-effect waves-light btn green accent-4 disabled'>Added to My Teams</button>
          }
          <div className = 'row center'>
            <div className = 'col s12 m6'>
              <img src = {data.crestUrl} className = 'team-info-crest' />
            </div>
            <div className = 'team-info col s12 m6'>
              <div className = 'row left-align'>
                <span className = 'col s12 m4 flow-text bold'>Name:</span>
                <span className = 'col s12 l8 flow-text'>{data.name}</span>
              </div>
              <div className = 'row left-align'>
                <span className = 'col s12 m4 flow-text bold'>Address:</span>
                <span className = 'col s12 l8 flow-text'>{data.address}</span>
              </div>
              <div className = 'row left-align'>
                <span className = 'col s12 m4 flow-text bold'>Phone:</span>
                <span className = 'col s12 l8 flow-text'>{data.phone}</span>
              </div>
              <div className = 'row left-align'>
                <span className = 'col s12 m4 flow-text bold'>Website:</span>
                <span className = 'col s12 l8 flow-text'>{data.website}</span>
              </div>
              <div className = 'row left-align'>
                <span className = 'col s12 m4 flow-text bold'>Email:</span>
                <span className = 'col s12 l8 flow-text'>{data.email}</span>
              </div>
              <div className = 'row left-align'>
                <span className = 'col s12 m4 flow-text bold'>Founded:</span>
                <span className = 'col s12 l8 flow-text'>{data.founded}</span>
              </div>
            </div>
          </div>
          
          <div className = 'divider' />
          
          <div className = 'section'>
            <Table title = 'Squad' th = {th} responsive = {true}>
              <SquadRow squad = {squad} />
            </Table>
          </div>
        </div>
      }

    </>
  )
}

export default Teams