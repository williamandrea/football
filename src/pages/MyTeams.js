import React, {useEffect, useState} from 'react'
import ReactLoading from 'react-loading'
import {Table, MyTeamsRow} from '../components'
import {getAll} from '../data/db'
import {useHistory} from 'react-router-dom'

const th = [
  '#',
  'Teams'
]
const MyTeams = () => {
  const history = useHistory()
  const [isLoading, setLoading] = useState(false)
  const [data, setData] = useState([])

  useEffect(() => {
    window.scrollTo(0, 0)
    getSavedTeams()
  }, [])
  
  const getSavedTeams = () => {
    getAll().then((teams) => {
      setLoading(true)
      setData(teams)
    }).finally(() => {
      setLoading(false)
    })
  }

  const handleRowClick = (id) => {
    history.push(`/teams/${id}`)
  }

  return (
    <>
      <h2 id = 'page-title' className = 'header black-text'>My Teams</h2>
      <div className = 'divider' />
      {
        console.log(data)
      }
      {
        isLoading? 
        <ReactLoading className = 'center-align' type = 'bubbles' color = 'black' />:
        <div className = 'section'>
          <Table th = {th} responsive = {false}>
            <MyTeamsRow data = {data} handleRowClick = {handleRowClick} />
          </Table>
        </div>
      }
    </>
  )
}

export default MyTeams