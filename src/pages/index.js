import Standings from './Standings'
import Teams from './Teams'
import MyTeams from './MyTeams'

export {Standings, Teams, MyTeams}