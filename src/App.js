import React from 'react'
import {Standings, Teams, MyTeams} from './pages'
import {Navbar, Footer} from './components'
import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom'

const App = () => {
  return (
    <>
    <Navbar />
    <div className = 'container page-container'>
      <Router>
        <Switch>
          <Route exact path = '/'>
            <Redirect to = '/leaguestandings' />
          </Route>
          <Route exact path = '/leaguestandings' component = {Standings} />
          <Route exact path = '/teams/:id' component = {Teams} />
          <Route exact path = '/myteams' component = {MyTeams} />
        </Switch>
      </Router>
    </div>
    <Footer />
    </>
  )
}

export default App