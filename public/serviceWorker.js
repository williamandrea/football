const CACHE_NAME = 'soccerhub-v12'
const urlToCache = [
  '/',

  './icon.png',
  './index.html',
  './manifest.json',
  
  '../src/components/Footer.js',
  '../src/components/index.js',
  '../src/components/LeagueInfo.js',
  '../src/components/MyTeamsRow.js',
  '../src/components/Navbar.js',
  '../src/components/Select.js',
  '../src/components/SquadRow.js',
  '../src/components/StandingRow.js',
  '../src/components/Table.js',

  '../src/data/api.js',
  '../src/data/const.js',
  '../src/data/db.js',

  '../src/fonts/MaterialIcons.ttf',
  '../src/fonts/NotoSans.ttf',

  '../src/img/Bundesliga.png',
  '../src/img/Eredivisie.png',
  '../src/img/LaLiga.png',
  '../src/img/Ligue1.png',
  '../src/img/PremierLeague.png',
  '../src/img/SerieA.png',
  '../src/img/error.png',

  '../src/pages/index.js',
  '../src/pages/MyTeams.js',
  '../src/pages/Standings.js',
  '../src/pages/Teams.js',

  '../src/App.js',
  './src/index.css',
  './src/index.js',
]

self.addEventListener('install', (event) => {
  console.log('install service worker')
  event.waitUntil(
    caches.open(CACHE_NAME).then((cache) => {
      return cache.addAll(urlToCache)
    })
  )
})

self.addEventListener('fetch', (event) => {
  // const apiURL = 'https://api.football-data.org/v2'
  // if (event.request.url.indexOf(apiURL) > -1) {
  //   event.respondWith(
  //     caches.open(CACHE_NAME).then((cache) => {
  //       return fetch(event.request).then((response) => {
  //         cache.put(event.request.url, response.clone())
  //         return response
  //       })
  //     })
  //   )
  // }else {
  //   event.respondWith(
  //     caches.match(event.request).then((response) => {
  //       return response || fetch(event.request)
  //     })
  //   )
  // }
  event.respondWith(
    caches.match(event.request, {cacheName: CACHE_NAME})
      .then((response) => {
        if (response) {
          console.log('ServiceWorker fetch from cache')
          return response
        }
        const fetchRequest = event.request.clone()
        return fetch(fetchRequest).then((response) => {
          if (!response || response.status !== 200) {
            return response
          }
          const responseToCache = response.clone()
          caches.open(CACHE_NAME)
            .then((cache) => {
              cache.put(event.request, responseToCache)
            })
            console.log('ServiceWorker fetch from api')
            return response
        })
      })
  )
})

self.addEventListener('activate', (event) => {
  console.log('Aktivasi service worker baru')
  event.waitUntil(
    caches.keys().then((cacheNames) => {
      return Promise.all(
        cacheNames.map((cacheName) => {
          if (cacheName != CACHE_NAME) {
            console.log('ServiceWorker: cache', cacheName, 'dihapus')
            return caches.delete(cacheName)
          }
        })
      )
    })
  )
})

self.addEventListener('push', (event) => {
  let body;
  if (event.data) {
    body = event.data.text()
  }else {
    body = 'Push message no payload'
  }

  const options = {
    body: body,
    icon: './icon.png',
    vibrate: [100, 50, 100],
    data: {
      dateOfArrival: Date.now(),
      primaryKey: 1
    }
  }
  
  event.waitUntil(
    self.registration.showNotification('Soccer Hub', options)
  )
})

