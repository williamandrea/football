const webPush = require('web-push')

const vapidKeys = {
  'publicKey': 'BE9f3NO6Bz13WDwAFSQFU3G2ObX0d1wg7sXcrTjSHXzG9RuWafb0LJ1D-MrgNk4W5qmxlePNkverHJZ4yA2oDIU',
  'privateKey': 'JS9s3vossezQXi4isgIQkXq8rteBeXpKEak2htROXuo',
}

webPush.setVapidDetails(
  'mailto:example@yourdomain.org',
  vapidKeys.publicKey,
  vapidKeys.privateKey,
)

const pushSubscription = {
  'endpoint': 'https://fcm.googleapis.com/fcm/send/feeHg3sJTRs:APA91bHwEDvfblPpOqU40pUQ0hKOHXO_Y3zX0Yl0iZZ--TIvLsJR6fzcCWTLx1KL88_MYkpskyZgt14EkUBfrRgkB3-bFhmyq8gaL1ex6AsHD19MnG3XnKV8jbtQ0dcjnaGRcRz9-W80',
  'keys': {
    'p256dh': 'BCJgvqEUFcXLFNU7jcYTlot8oOopB9wLZMOtvfhawK1ELEWWz/ACg8qHuf4AbapzHLUZX0x5PQ2zVAXxxnWvJKM=',
    'auth': 'fUvKQt0SYfEbCHRycGlNvQ==',
  }
}

const payload = 'Thank you for using Soccer Hub!!!'

const options = {
  gcmAPIKey: '137159101162',
  TTL: 60,
}

webPush.sendNotification(
  pushSubscription,
  payload,
  options,
)